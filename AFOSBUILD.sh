rm -rf /opt/ANDRAX/jadx

mkdir /opt/ANDRAX/jadx

wget https://github.com/skylot/jadx/releases/download/v1.5.0/jadx-1.5.0.zip

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Download jadx... PASS!"
else
  # houston we have a problem
  exit 1
fi

unzip -o -d /opt/ANDRAX/jadx jadx-1.5.0.zip

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Extract jadx... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
